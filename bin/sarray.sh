#!/bin/bash
# VERSION: 1.2
# AUTHORS: Patrice Dehais - SIGENAE Toulouse (sigenae-support@listes.inra.fr)
# COPYRIGHT: 2017 INRA
# LICENSE: GNU GPLv3

# This script is a wrapper for sbatch to create a job array that will execute in parallel SHELL lines from a file

function error ()
{
    echo 1>&2 ERROR: "$@"
    echo 1>&2 "Usage: $0 [sbatch options] shell_command_file"
    echo 1>&2 "Extra option: --%=XXX where XXX is the number of simultaneously running tasks from the job array"
    echo 1>&2 "Read man sbatch to have more information about available sbatch options"
    echo 1>&2 "Command line example: $0 -J MyFirstSarrayJob myCmdLines.txt"
    echo 1>&2 "Each line in myCmdLines.txt will be run in parallel"
    echo 1>&2 "Each line is a SHELL command line, thus eventually with several commands delimited by semicolons, &&, etc."
    echo 1>&2 "Example2: $0 -J jobname -o myDIR/slurm-%A_%a.out -e myDIR/slurm-%A_%a.err -c 2 --mem-per-cpu=10G --%=4 myCmdLines.txt"
    echo 1>&2 "Explanations: stdout and stderr redirected, more memory requested, 2 cpus per task, and max 4 tasks in parallel"
    exit 1
}

function father_call ()
{
    if [ $# -eq 0 ] ; then
		error "missing parameter"
    fi
    argv=("$@")
    n=$(($#-1))
    if ! [ -e "${argv[$n]}" ] ; then
	error "can't find SHELL command file: ${argv[$n]}"
    fi
    cmd_file=`readlink -f "${argv[$n]}"`
    # remove last parameter (the command file) to have only sbatch options now
    argv=("${argv[@]:0:$n}")
    if [ ! -e "$cmd_file" -o -d "$cmd_file" ] ; then
		error "last parameter $cmd_file should be a file"
    fi
    m=`cat "$cmd_file"|wc -l`
    if [ $? -ne 0 ] ; then
		error "can't retrieve number of lines of file $cmd_file"
    fi
    if [ $m -eq 0 ] ; then
		error "$cmd_file is empty"
    fi

	# submit batch
	sbatch "${argv[@]}" --array=1-$m$max_parallel_tasks $0 --sarray_command_file="$cmd_file"

    exit 0
}

function child_call ()
{
	[ -z "$SLURM_ARRAY_TASK_ID" ] && error "no in job array context"
	cmd_file=`readlink -f "$1"`
    if [ ! -e $cmd_file -o -d $cmd_file ] ; then
		error "$cmd_file should be a file"
    fi
    sed -n ${SLURM_ARRAY_TASK_ID}p "$cmd_file"|$SHELL
    exit $?
}


cmd_file=""
# check context : from shell or for batch (parametter --sarray_command_file=fffff set)
# if launched from shell, command file is specified as LAST parameter
# if launched via sbatch, command file is specified through parameter --sarray_command_file
i=0
for p in "$@"; do
	# specific parameter for child call
	if [ "${p%=*}" == "--sarray_command_file" ] ; then
		cmd_file="${p#--sarray_command_file=*}"
		break 
	fi
	if [ "$p" == "-a" -o "${p%=*}" == "--array" ] ; then
		error "parameter --array (-a) is computed by $0, and thus should not be specified here"
	fi
	# extra parameter for parallel tasks limit
	if [ "${p%=*}" == "--%" ] ; then
		max_parallel_tasks=${p#--%=*}
		re='^[0-9]+$'
		if ! [[ $max_parallel_tasks =~ $re ]] ; then
			error "bad parallel tasks limit option, not an integer: $p"
		fi
		if [ $max_parallel_tasks -le 0 ] ; then
			error "bad parallel tasks limit option, must be greater than 0: $p"
		fi
		max_parallel_tasks="%$max_parallel_tasks"
		# remove it from sbatch options
		continue
	fi
	argv[$i]="$p"
	i=$(($i+1))
done

if [ -z "$cmd_file" ] ; then 
    father_call "${argv[@]}"
else
    child_call "$cmd_file"
fi

