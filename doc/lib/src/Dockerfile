FROM ubuntu:18.04
MAINTAINER sigenae "sigenae-support@listes.inra.fr"
ENV REFRESHED_AT 2019-05-20

## -- install packages -- ##
ENV DEBIAN_FRONTEND teletype
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils\
	bedtools \
	berkeley-express \
	bioperl \
	bioperl-run \
	blast2 \
	bowtie2 \
	bwa \
	bzip2 \
	cd-hit \
	cmake \
	csh \
	curl \
	cutadapt \
	dc \
	default-jre \
	emboss \
	exonerate \
	g++ \
	gcc \
	git \
	hmmer \
	jellyfish \
	khmer \
	libc6-i386 \
	libcompress-raw-zlib-perl \
	libgetopt-long-descriptive-perl \
	libipc-run-perl  \
	libjson-perl \
	libncurses5-dev \
	libpod2-base-perl \
	libterm-extendedcolor-perl \
	cpanminus \
	make \
	nano \
	ncbi-blast+ \
	ncbi-tools-bin \
	parallel \
	patch \
	python3-setuptools \
	python \
	python-biopython \
	python-scipy \
	r-base \
	r-cran-ggplot2 \
	rna-star \
	rsync \
	ruby-dev \
	samtools \
	transdecoder \
	wget \
	zlib1g-dev \
&& rm -rf /var/lib/apt/lists/*

## -- Last version of List::Util Perl module -- ##
RUN cpanm List::Util

## -- create links -- ##
RUN cd /usr/bin \
	&& ln -s $PWD/env /bin/env \
	&& ln -s $PWD/python /bin/python \
	&& ln -s $PWD/cdhit /usr/local/bin/cd-hit \
	&& ln -s $PWD/cdhit-est /usr/local/bin/cd-hit-est \
	&& ln -s $PWD/berkeley-express /usr/local/bin/express

## -- set ENV -- ##
ENV PATH /usr/lib/khmer/bin:$PATH
ENV USER root

## -- install velvet/oases with long kmers -- ##
RUN cd /usr/local/src \
	&& git clone --recursive https://github.com/dzerbino/velvet.git \
	&& cd velvet \
	&& make 'MAXKMERLENGTH=70' 'LONGSEQUENCES=1' \
	&& ln -s $PWD/velveth /usr/local/bin/velveth \
	&& ln -s $PWD/velvetg /usr/local/bin/velvetg 
RUN cd /usr/local/src \
	&& git clone --recursive https://github.com/dzerbino/oases  \
	&& cd oases && make 'MAXKMERLENGTH=70' 'LONGSEQUENCES=1' \
 	&& ln -s $PWD/oases /usr/local/bin/oases

## -- install trimGalore -- ##
RUN cd /usr/local/src \
	&& curl -SL https://github.com/FelixKrueger/TrimGalore/archive/0.4.2.tar.gz | tar -xz \
	&& cd TrimGalore-0.4.2 \
	&& ln -s $PWD/trim_galore /usr/local/bin/trim_galore

## -- install blat -- ##
RUN cd /usr/local/bin \
	&& wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/blat/blat \
	&& chmod +x blat

## -- install fastq_illumina_filter -- ##
RUN cd /usr/local/bin \
	&& wget http://cancan.cshl.edu/labmembers/gordon/fastq_illumina_filter/release/0.1/fastq_illumina_filter-Linux-x86_64 \
	&& chmod +x fastq_illumina_filter-Linux-x86_64 \
	&& ln -s fastq_illumina_filter-Linux-x86_64 fastq_illumina_filter

## -- install seqclean -- ##
## unable to dowload archives from occams.dfci.harvard.edu with curl; use wget ##
## the chmod command allows a local user to run seqclean
RUN cd /usr/local/src \
	&& wget ftp://occams.dfci.harvard.edu/pub/bio/tgi/software/seqclean/seqclean_x86_64.tar.gz \
	&& tar -xzf seqclean_x86_64.tar.gz && rm -f seqclean_x86_64.tar.gz \
	&& ln -s $PWD/seqclean/seqclean /usr/local/bin/seqclean \
	&& chmod +rx /usr/local/src/seqclean/bin/*
ENV PATH /usr/local/src/seqclean/bin:$PATH

## -- install tgicl -- ##
RUN cd /usr/local/src \
	&& wget ftp://occams.dfci.harvard.edu/pub/bio/tgi/software/tgicl/tgicl_linux.tar.gz  \
	&& tar -xzf tgicl_linux.tar.gz && rm -f tgicl_linux.tar.gz \
	&& wget ftp://occams.dfci.harvard.edu/pub/bio/tgi/software/tgicl/gclib.tar.gz \
	&& tar -xzf gclib.tar.gz && rm -f gclib.tar.gz \
	&& wget ftp://occams.dfci.harvard.edu/pub/bio/tgi/software/tgicl/zmsort.tar.gz \
	&& tar -xzf zmsort.tar.gz && rm -f zmsort.tar.gz \
	&& curl -SL http://seq.cs.iastate.edu/CAP3/cap3.linux.x86_64.tar | tar -x \
	&& cd zmsort && make && cp zmsort ../tgicl_linux/bin && cd .. \
	&& cp CAP3/cap3 tgicl_linux/bin \
	&& ln -s $PWD/tgicl_linux/tgicl /usr/local/bin/tgicl
ENV PATH /usr/local/src/tgicl_linux/bin:$PATH
ENV PERL5LIB /usr/local/src/tgicl_linux:$PERL5LIB

## -- install salmon -- ##
RUN cd /usr/local/src \
	&& mkdir salmon-0.13.1_linux_x86_64 \
	&& curl -SL https://github.com/COMBINE-lab/salmon/releases/download/v0.13.1/salmon-0.13.1_linux_x86_64.tar.gz | tar -xz -C salmon-0.13.1_linux_x86_64 --strip-components 1 \
	&& cd salmon-0.13.1_linux_x86_64 \
	&& ln -s $PWD/bin/salmon /usr/local/bin/salmon 

## -- install trinity -- ##
RUN cd /usr/local/src \
	&& curl -SL https://github.com/trinityrnaseq/trinityrnaseq/archive/Trinity-v2.8.4.tar.gz | tar -xz \
	&& cd trinityrnaseq-Trinity-v2.8.4 \
	&& make && make plugins \
	&& ln -s $PWD/Trinity /usr/local/bin/Trinity \
	&& ln -s $PWD/util/insilico_read_normalization.pl /usr/local/bin/insilico_read_normalization.pl \
	&& rm -rf $PWD/sample_data
ENV PATH /usr/local/src/trinityrnaseq-Trinity-v2.8.4/trinity-plugins/BIN:$PATH

## -- install transrate -- ##
RUN cd /usr/local/src \
        && curl -SL https://bintray.com/artifact/download/blahah/generic/transrate-1.0.3-linux-x86_64.tar.gz | tar -xz \
        && cd transrate-1.0.3-linux-x86_64 \
        && ./transrate --install-deps ref \
	&& ln -s $PWD/transrate /usr/local/bin/transrate \
	&& rm -f bin/librt.so.1

## -- install busco -- ##
RUN cd /usr/local/src \
	&& git clone --recursive https://gitlab.com/ezlab/busco.git \
	&& cd busco && python setup.py install \ 
	&& mkdir datasets && cd datasets \
	&& curl -SL http://busco.ezlab.org/v2/datasets/vertebrata_odb9.tar.gz | tar -xz \
	&& curl -SL http://busco.ezlab.org/v2/datasets/metazoa_odb9.tar.gz | tar -xz \
	&& curl -SL http://busco.ezlab.org/v2/datasets/eukaryota_odb9.tar.gz | tar -xz \
	&& curl -SL http://busco.ezlab.org/v2/datasets/arthropoda_odb9.tar.gz | tar -xz \
	&& curl -SL http://busco.ezlab.org/v2/datasets/fungi_odb9.tar.gz | tar -xz \
	&& curl -SL http://busco.ezlab.org/v2/datasets/bacteria_odb9.tar.gz | tar -xz

RUN cd /usr/local/src/busco \
	&& mv config/config.ini.default config/config.ini \
	&& sed -i -e 's|/home/osboxes.*|/usr/bin/|' config/config.ini \
	&& chmod +x scripts/run_BUSCO.py scripts/generate_plot.py \
	&& ln -s $PWD/scripts/run_BUSCO.py /usr/local/bin/run_BUSCO.py \
	&& ln -s $PWD/scripts/generate_plot.py /usr/local/bin/generate_plot.py

## -- loading the adapter and contamination databases for TSA compliance -- ##
RUN mkdir /usr/local/src/banks \
	&& cd /usr/local/src/banks \
	&& wget ftp://ftp.ncbi.nlm.nih.gov/pub/kitts/adaptors_for_screening_euks.fa \
	&& wget ftp://ftp.ncbi.nlm.nih.gov/pub/kitts/contam_in_euks.fa.gz && gzip -d contam_in_euks.fa.gz \
	&& wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/mito.nt.gz && gzip -d mito.nt.gz \
	&& wget ftp://ftp.ncbi.nlm.nih.gov/pub/kitts/rrna.gz && gzip -d rrna.gz \
	&& makeblastdb -in adaptors_for_screening_euks.fa -dbtype nucl \
	&& makeblastdb -in contam_in_euks.fa -dbtype nucl \
	&& makeblastdb -in mito.nt -dbtype nucl \
	&& makeblastdb -in rrna -dbtype nucl

## -- install DRAP -- ##
RUN cd /usr/local/src \
	&& git clone https://forgemia.inra.fr/cedric.cabau/drap.git drap \
	&& cd drap \
	&& ln -s $PWD/runCheck /usr/local/bin/runCheck \
	&& ln -s $PWD/runDrap /usr/local/bin/runDrap \
	&& ln -s $PWD/runMeta /usr/local/bin/runMeta \
	&& ln -s $PWD/runAssessment /usr/local/bin/runAssessment
ENV PERL5LIB /usr/local/src/drap/bin:$PERL5LIB
ENV PATH /usr/local/src/drap/bin:$PATH

## -- configure DRAP -- ##
RUN cd /usr/local/src/drap \
	&& sed -i -e 's|/path/to/adaptors_for_screening_euks.fa|/usr/local/src/banks/adaptors_for_screening_euks.fa|' cfg/drap.cfg \
	&& sed -i -e 's|/path/to/contam_in_euks.fa|/usr/local/src/banks/contam_in_euks.fa|' cfg/drap.cfg \
	&& sed -i -e 's|/path/to/mito.nt|/usr/local/src/banks/mito.nt|' cfg/drap.cfg \
	&& sed -i -e 's|/path/to/rrna|/usr/local/src/banks/rrna|' cfg/drap.cfg \
	&& sed -i -e 's|/path/to/busco_directory|/usr/local/src/busco/datasets|' cfg/drap.cfg \
	&& sed -i -e 's|type = .*|type = local|' cfg/drap.cfg \
	# correcting tgicl conflict version
	&& sed -i -e 's/tgicl -F/tgicl/' bin/runAssembly.sh \
	# patching transrate
	&& cd /usr/local/src/transrate-1.0.3-linux-x86_64/lib/app/lib/transrate \
	&& patch -b < /usr/local/src/drap/plugins/drap-transrate/transrate.patch
